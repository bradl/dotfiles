call plug#begin(stdpath('data') . '/plugged')
Plug 'tpope/vim-sensible'
Plug 'humanoid-colors/vim-humanoid-colorscheme'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()
set tabstop=2 shiftwidth=4 expandtab
let g:airline_powerline_fonts = 1
