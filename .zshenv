#if [ -n "$DESKTOP_SESSION" ]; then
#	eval $(gnome-keyring-daemon --start)
#	export SSH_AUTH_SOCK
#fi

# Setup path
if [ -e "$HOME/.cargo/bin" ]; then
    PATH="$HOME/.cargo/bin:$PATH"
fi
if [ -e "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi
if [ -e "$HOME/.config/emacs/bin" ]; then
    PATH="$HOME/.config/emacs/bin:$PATH"
fi
    


# Setup environment variables
if [ -e "$HOME/.cargo/env" ]; then
    source "$HOME/.cargo/env"
fi

export EDITOR=nvim
export SSH_ASKPASS=gnome-keyring


# Setup aliases
alias vim=nvim
alias vi=vim
alias hc=herbstclient
alias irssi="irssi --home=~/.config/irssi"

# source ~/.profile
