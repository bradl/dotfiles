#!/usr/bin/env bash
DOTSURL=git@gitlab.com:bradl/dotfiles

# check if git exists
if ! type git > /dev/null; then
    echo "Please install git"
    exit
fi

if ! type ssh-keygen > /dev/null; then
    echo "Please install openssh"
    exit
fi

ssh git@gitlab.com 2>&1 | grep Welcome
while [[ $? != 0 ]]; do
    echo "Please add your ssh key to gitlab."
    if [[ ! -f "$HOME/.ssh/id_rsa.pub" ]]; then
        echo "Generating key with ssh-keygen:"
        ssh-keygen -f "$HOME/.ssh/id_rsa"
    fi
    echo "SSH key to copy:"
    cat "$HOME/.ssh/id_rsa.pub"
    echo "Copy to https://gitlab.com/-/profile/keys"
    read -n 1 -p "Press any key to continue..."
    ssh git@gitlab.com 2>&1 | grep Welcome
done

mkdir -p "$HOME/src"
git clone --bare "$DOTSURL" "$HOME/src/dotfiles"
git --git-dir="$HOME/src/dotfiles" --work-tree="$HOME" checkout

if [[ $? != 0 ]]; then
    echo "Failed to checkout dotfiles, please backup any files mentioned if you want to keep your version, and remove the files before running:"
    echo 'git --git-dir="$HOME/src/dotfiles" --work-tree="$HOME" checkout'
fi
